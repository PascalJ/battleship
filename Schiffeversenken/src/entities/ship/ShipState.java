/**
 * The state of a ship or a part of a ship
 *
 * User: Pascal
 * Version: 1.0
 * Date: 01.03.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 * 1.0 (01.03.13)
 *  - Add values (Ok, Damaged, Sunk).
 */
package entities.ship;

/**
 * The state of a ship or a part of a ship
 *
 * @author Pascal
 * @version 1.0
 */
public enum ShipState {
    Ok,
    Damaged,
    Sunk
}
