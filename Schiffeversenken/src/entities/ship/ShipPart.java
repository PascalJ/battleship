/**
 * A part of a ship
 *
 * User: pascal
 * Version: 1.1
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 *  - extend 'EntityBase'
 * 1.0 (16.04.13)
 *  - Add StateAsChar method.
 * 1.1 (18.04.13)
 *  - Replace chars with constants.
 *  - Add hide mode.
 */
package entities.ship;

import entities.EntityBase;
import other.Const;

/**
 * A part of a ship
 *
 * @author pascal
 * @version 1.1
 */
public class ShipPart extends EntityBase
{
    /**
     * The state of the ship part
     */
    public ShipState State;

    /**
     * Initialize a new ship part.
     */
    public ShipPart()
    {
        super();
    }

    /**
     * Initialize the entity.
     */
    @Override
    public void Initialize()
    {
        State = ShipState.Ok;
    }

    /**
     * Translate the field into a char, so it can be printed out in
     * the console
     *
     * @return Returns a char with the field data.
     */
    public Character StateAsChar(boolean hide)
    {
        switch (State)
        {
            case Damaged:
                return Const.Circle;
            case Sunk:
                return Const.Diamond;
            default:
                if (hide)
                    return '~';
                else
                    return Const.Rectangle;
        }
    }

    /**
     * Update the entity object.
     */
    @Override
    public void Update()
    {

    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose()
    {
    }
}
