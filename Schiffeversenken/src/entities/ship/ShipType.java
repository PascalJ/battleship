/**
 * The enum of ship types.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 01.03.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 * 1.0 (01.03.13)
 *  - Add values (Battleship, Cruiser, Destroyer, Submarine).
 */
package entities.ship;

/**
 * The enum of ship types.
 *
 * @author Pascal
 * @version 1.0
 */
public enum ShipType {
    Battleship,
    Cruiser,
    Destroyer,
    Submarine,
    YellowSubmarine
}
