/**
 * A ship with parts, a position and state.
 *
 * User: Pascal
 * Version: 1.2
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 * 1.0 (15.04.13)
 *  - Add constructors
 *  - Add information properties (length, vertical, position, ...)
 *  - Add IsSunk method
 * 1.1 (18.04.13)
 *  - Refactor methods
 */
package entities.ship;

import entities.EntityBase;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A ship with parts, a position and state.
 *
 * @author Pascal
 * @version 1.2
 */
public class Ship extends EntityBase
{
    public ShipType Type;
    public ShipState State;
    public List<ShipPart> Parts;
    public int Length;
    public boolean Vertical;
    public Point Position;

    /**
     * Create a new Ship object
     *
     * @param type The ship type
     * @param length The length of a ship
     */
    public Ship(ShipType type, int length)
    {
        super();
        Type = type;
        Length = length;
        Initialize();
    }

    /**
     * Create a new Ship object
     *
     * @param type The ship type
     * @param length The length of a ship
     * @param vertical Should the ship placed vertical or not?
     */
    public Ship(ShipType type, int length, boolean vertical)
    {
        this(type, length);
        Vertical = vertical;
        State = ShipState.Ok;
    }

    /**
     * Create a new Ship object
     *
     * @param type The ship type
     * @param length The length of a ship
     * @param position The ship position
     * @param vertical Should the ship placed vertical or not?
     */
    public Ship(ShipType type, int length, Point position, boolean vertical)
    {
        this(type, length, vertical);
        Position = position;
    }

    /**
     * Initialize the entity.
     */
    @Override
    public void Initialize()
    {
        // Set a new position if it is not set in the constructor.
        if (Position == null)
            Position = new Point(0, 0);

        Parts = new ArrayList<ShipPart>();
        for (int i = 0; i < Length; i++)
        {
            Parts.add(new ShipPart());
        }
    }

    /**
     * Check whether all ship part are damaged.
     * @return if all Parts are damaged the ship sink.
     */
    public boolean IsSunk()
    {
        boolean ret = true;
        for (ShipPart part : Parts)
        {
            if (part.State != ShipState.Damaged)
                ret = false;
        }
        return ret;
    }

    /**
     * Marks the parts of the ship as sunk.
     */
    public void Sink()
    {
        State = ShipState.Sunk;
        for (ShipPart part : Parts)
        {
            part.State = ShipState.Sunk;
        }
    }

    /**
     * Update the entity object.
     */
    @Override
    public void Update()
    {
    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose()
    {
        Parts.clear();
    }
}
