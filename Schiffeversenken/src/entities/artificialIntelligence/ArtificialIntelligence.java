/**
 * Base Class for an artificial Intelligence
 *
 * User: Pascal
 * Version: 1.0
 * Date: 14.04.13
 *
 * Changes:
 * 0.1 (13.04.13)
 *  - First implementation
 * 1.0 (14.04.13)
 *  - Add interface functions.
 */
package entities.artificialIntelligence;

import entities.EntityBase;
import entities.Map.FieldMap;

import java.awt.*;

/**
 * Base Class for an artificial Intelligence
 *
 * @author Pascal
 * @version 1.0
 */
public abstract class ArtificialIntelligence extends EntityBase
{
    /**
     * Map of the ai.
     */
    public FieldMap Map;

    /**
     * Calculate missile
     * @param map The targeting map
     * @return Returns missile point.
     */
    public abstract Point DoWork(FieldMap map);

    /**
     * Determine the state of the ai and generate state sentence
     * @return Returns strings with states of the ai (winning, loosing, etc.)
     */
    public abstract String GetState();

    /**
     * Set the ships of the ai.
     */
    public abstract void SetShips();
}
