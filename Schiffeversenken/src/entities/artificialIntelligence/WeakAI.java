/**
 * Weak AI
 *
 * User: Pascal
 * Version: 1.0
 * Date: 17.04.13
 *
 * Changes:
 * 0.1 (15.04.13)
 *  - First implementation
 * 1.0 (17.04.13)
 *  - Implement DoWork(), SetShips() and GetState() methods.
 */
package entities.artificialIntelligence;

import entities.Map.FieldMap;
import entities.ship.Ship;
import other.Randomizer;

import java.awt.Point;
import java.util.List;
import java.util.ArrayList;

/**
 * Weak AI
 *
 * @author Pascal
 * @version 1.0
 */
public class WeakAI extends ArtificialIntelligence
{
    /**
     * Calculate missile
     *
     * @return Returns missile point.
     */
    @Override
    public Point DoWork(FieldMap map)
    {
        return new Point(Randomizer.nextInt(Map.MapSize.x -1), Randomizer.nextInt(Map.MapSize.y -1));
    }

    /**
     * Determine the state of the ai and generate state sentence
     *
     * @return Returns strings with states of the ai (winning, loosing, etc.)
     */
    @Override
    public String GetState() {
        String ret;
        List<String> winSentences = new ArrayList<String>();
        List<String> looseSentences = new ArrayList<String>();
        List<String> normalSentences = new ArrayList<String>();

        winSentences.add("I'm the best. :P");
        winSentences.add("I thought I am winning.");
        winSentences.add("Are you blind? ^^");
        winSentences.add("There's nothing better than me, except Chuck.");
        winSentences.add("Boring. :|");
        winSentences.add("Problem?");

        normalSentences.add("Do you know, the game exists more than 100 years.");
        normalSentences.add("You're a good player.");
        normalSentences.add("Hmmm.");
        normalSentences.add("Lol.");

        looseSentences.add("A bad game, I'm not winning.");
        looseSentences.add("What, you're better than me. O.o");
        looseSentences.add("This cannot be.");

        if (Map.getMoves() == 0)
        {
            ret = "Let the game begin.";
        }
        else if (Map.getHasHit())
        {
            ret = normalSentences.get(Randomizer.nextInt(normalSentences.size() -1));
        }
        else if (Map.Defeated())
        {
            ret = looseSentences.get(Randomizer.nextInt(looseSentences.size() -1));
        }
        else
        {
            int sunkShips = 0;
            for (Ship ship : Map.ships)
                if (ship.IsSunk())
                    sunkShips++;

            if (sunkShips > 8)
                ret = looseSentences.get(Randomizer.nextInt(looseSentences.size() -1));
            else if (sunkShips < 8)
                ret = winSentences.get(Randomizer.nextInt(winSentences.size() -1));
            else
                ret = normalSentences.get(Randomizer.nextInt(normalSentences.size() -1));
        }

        return ret;
    }

    /**
     * Set the ships of the ai.
     */
    @Override
    public void SetShips() {
        Map.SetShipsRandomly();
    }

    /**
     * Initialize the entity.
     */
    @Override
    public void Initialize() {
        Map = new FieldMap();
    }

    /**
     * Update the entity object.
     */
    @Override
    public void Update() {
        Map.Update();
    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose() {
        Map.Dispose();
    }
}