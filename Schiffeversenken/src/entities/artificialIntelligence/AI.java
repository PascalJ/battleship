/**
 * AI class which controls all ai levels.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 14.04.13
 *
 * Changes:
 * 0.1 (15.04.13)
 *  - First implementation
 * 1.0 (16.04.13)
 *  - Implement processing of ai levels.
 */
package entities.artificialIntelligence;

import entities.Map.FieldMap;

import java.awt.*;

/**
 * AI class which controls all ai levels.
 *
 * @author Pascal
 * @version 1.0
 */
public class AI extends ArtificialIntelligence
{
    public AILevel Level = AILevel.Weak;
    WeakAI weakAI;
    HardAI hardAI;

    public AI (AILevel level)
    {
        super();
        Level = level;
    }

    public FieldMap GetMap()
    {
        switch (Level)
        {
            case Hard:
                return hardAI.Map;
            default: // Weak
                return weakAI.Map;
        }
    }

    /**
     * Calculate missile
     * @param map The targeting map
     * @return Returns missile point.
     */
    @Override
    public Point DoWork(FieldMap map) {
        switch (Level)
        {
            case Hard:
                return hardAI.DoWork(map);
            default: // Weak
                return weakAI.DoWork(map);
        }
    }

    /**
     * Determine the state of the ai and generate state sentence
     *
     * @return Returns strings with states of the ai (winning, loosing, etc.)
     */
    @Override
    public String GetState() {
        switch (Level)
        {
            case Hard:
                return hardAI.GetState();
            default: // Weak
                return weakAI.GetState();
        }
    }

    /**
     * Set the ships of the ai.
     */
    @Override
    public void SetShips() {
        switch (Level)
        {
            case Hard:
            {
                hardAI.SetShips();
                break;
            }
            default: // Weak
            {
                weakAI.SetShips();
                break;
            }
        }
    }

    /**
     * Initialize the entity.
     */
    @Override
    public void Initialize() {
        weakAI = new WeakAI();
        hardAI = new HardAI();
    }

    /**
     * Update the entity object.
     */
    @Override
    public void Update() {
        switch (Level)
        {
            case Hard:
            {
                hardAI.Update();
                break;
            }
            default: // Weak
            {
                weakAI.Update();
                break;
            }
        }
    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose() {
        hardAI.Dispose();
        weakAI.Dispose();
    }
}
