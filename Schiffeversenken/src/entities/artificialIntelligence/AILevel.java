/**
 * Level of a ai.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 15.04.13
 *
 * Changes:
 * 1.0 (15.04.13)
 *  - Add levels weak and hard.
 */
package entities.artificialIntelligence;

/**
 * Level of a ai.
 *
 * @author Pascal
 * @version 1.0
 */
public enum AILevel
{
    Weak,
    Hard
}
