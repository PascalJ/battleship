/**
 * The state of a field on a map.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 07.03.13
 *
 * Changes:
 * 0.1 (07.03.13)
 *  - First implementation
 * 1.0 (07.03.13)
 *  - Add values (Nothing, Coast, Occupied, Bombed).
 */
package entities.Map;

/**
 * The state of a field on a map.
 *
 * @author Pascal
 * @version 1.0
 */
public enum FieldState
{
    Nothing,
    Coast,
    Occupied,
    Bombed
}
