/**
 * A Field of a map with a state.
 *
 * User: Pascal
 * Version: 1.1
 * Date: 17.04.13
 *
 * Changes:
 * 0.1 (07.03.13)
 *  - First implementation
 * 1.0 (15.04.13)
 *  - Add FieldAsChar method.
 * 1.1 (17.04.13)
 *  - Replace unicode characters with constants.
 */
package entities.Map;

import entities.EntityBase;
import other.Const;

/**
 * A Field of a map with a state.
 *
 * @author Pascal
 * @version 1.1
 */
public class Field extends EntityBase
{
    public FieldState State;

    @Override
    public void Initialize()
    {
        State = FieldState.Nothing;
    }

    /**
     * Translate the field into a char, so it can be printed out in
     * the console
     *
     * @return Returns a char with the field data.
     */
    public char FieldAsChar(boolean hide)
    {
        switch (State)
        {
            case Coast:
                return Const.Filter;
            case Occupied:
            {
                if (hide)
                    return '~';
                else
                    return 'O';
            }
            case Bombed:
                return 'o';
            default:
                return '~';
        }
    }

    @Override
    public void Update()
    {
    }

    @Override
    public void Dispose()
    {
    }
}
