/**
 * Base class of the game data objects.
 *
 * User: Pascal
 * Version: 0.1
 * Date: 01.03.13
 *
 * Changes:
 * 1.0 (01.03.13)
 *  - First implementation
 *  - Add interface 'IDisposable'
 *  - Add abstract void 'Update()' and 'Initialize()'.
 */
package entities;

import other.IDisposable;

/**
 * Base class of the game data objects.
 *
 * @author Pascal
 * @version 1.0
 */
public abstract class EntityBase
implements IDisposable
{
    /**
     * Create a new <code>EntityBase</code> object
     */
    public EntityBase()
    {
        Initialize();
    }

    /**
     * Initialize the entity.
     */
    public abstract void Initialize();

    /**
     * Update the entity object.
     */
    public abstract void Update();
}
