/**
 * An interface to centralize the game functionality.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 02.03.13
 *
 * Changes:
 * 0.1 (28.02.13)
 *  - First implementation
 * 1.0 (02.03.13)
 *  - Add interface functions (Initialize(), LoadContent(), Run(), Update() and Render).
 *  - Add IDisposable interface.
 */
package game;

import other.IDisposable;

/**
 * An interface to centralize the game functionality.
 *
 * @author Pascal
 * @version 0.1
 */
public interface IGame extends IDisposable
{
    /**
     * Initialize the game object.
     */
    void Initialize();

    /**
     * Load the game content.
     */
    void LoadContent();

    /**
     * Initializes and start a rudimentary game loop.
     * @return the next game state.
     */
    String Run();

    /**
     * Handles game updates.
     */
    void Update();

    /**
     * Handles render calls.
     */
    void Render();
}
