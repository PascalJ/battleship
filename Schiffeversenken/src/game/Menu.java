/**
 * The menu game state.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 15.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 *  - implement menu
 * 1.0 (14.04.13)
 *  - Fix some minor bugs.
 *  - Add title
 */
package game;

import io.in;
import io.out;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The menu game state.
 *
 * @author Pascal
 * @version 1.0
 */
public class Menu
implements IGame
{
    public boolean IsRunning;
    String nextGameState;
    List<String> mainMenu;

    /**
     * Initialize the game object.
     */
    @Override
    public void Initialize()
    {
        IsRunning = true;
        mainMenu = new ArrayList<String>();
    }

    /**
     * Load the game content.
     */
    @Override
    public void LoadContent()
    {
        mainMenu.add("Play");
        mainMenu.add("Options");
        mainMenu.add("End");
    }

    /**
     * Initializes and start a rudimentary game loop.
     * @return the next game state.
     */
    @Override
    public String Run()
    {
        Initialize();
        LoadContent();

        while (IsRunning)
        {
            Render();
            Update();
        }

        Dispose();
        return nextGameState;
    }

    /**
     * Handles game updates.
     */
    @Override
    public void Update()
    {
        Map<String, Integer> menuEntry = in.getMenuInput(mainMenu, true);

        if (menuEntry.containsKey("Play") &&
            mainMenu.get(menuEntry.get("Play")).equalsIgnoreCase("play"))
        {
            IsRunning = false;
            nextGameState = "Game";
        }
        else if (menuEntry.containsKey("Options") &&
                 mainMenu.get(menuEntry.get("Options")).equalsIgnoreCase("options"))
        {
            IsRunning = false;
            nextGameState = "Options";
        }
        else
        {
            IsRunning = false;
            nextGameState = "Exit";
        }

        // Clear the menuEntry
        menuEntry.clear();
    }

    /**
     * Handles render calls.
     */
    @Override
    public void Render()
    {
        // Battleship
        System.out.printf("__________         __    __  ,__                ,__    ,__               \n");
        System.out.printf("\\______   \\_____ _/  |__/  |_|  |   ____   _____|  |__ |__|_____       \n");
        System.out.printf(" |    |  _/\\__  \\\\   __\\   __\\  | _/ __ \\ /  ___/  |  \\|  \\____ \\ \n");
        System.out.printf(" |    |   \\ / __ \\|  |  |  | |  |_\\  ___/ \\___ \\|   Y  \\  |  |_> > \n");
        System.out.printf(" |______  /(____  /__|  |__| |____/\\___  >____  >___|  /__|   __/       \n");
        System.out.printf("        \\/      \\/                     \\/     \\/     \\/   |__|      \n\n");

        out.WriteMenu(mainMenu);
    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose()
    {
        mainMenu.clear();
    }
}
