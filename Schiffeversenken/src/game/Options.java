/**
 * Options game state to configure the game.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (18.04.13
 *  - First implementation
 */
package game;

import entities.artificialIntelligence.AILevel;
import io.FileFunctions;
import other.TryParse;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Options game state to configure the game.
 *
 * @author Pascal
 * @version 1.0
 */
public class Options implements IGame
{
    Scanner scan;
    public static AILevel AIlevel;
    public static Point MapSize;
    private boolean isRunning;

    /**
     * Initialize the game object.
     */
    @Override
    public void Initialize() {
        scan = new Scanner(System.in);
        isRunning = true;
        MapSize = new Point(10, 10);
        AIlevel = AILevel.Weak;
    }

    /**
     * Load options from a file and write it to properties
     */
    public static void ReadOptionsToProperties()
    {
        Map<String, String> options = GetOptions("./options.txt");
        if (options.get("AIlevel").equalsIgnoreCase("hard"))
        { AIlevel = AILevel.Hard; }
        else { AIlevel = AILevel.Weak; }

        MapSize = new Point(Integer.parseInt(options.get("MapSize").split(":")[0].toString()),
                            Integer.parseInt(options.get("MapSize").split(":")[1].toString()) );

    }

    /**
     * Load the game content.
     */
    @Override
    public void LoadContent() {
        ReadOptionsToProperties();
    }

    /**
     * Initializes and start a rudimentary game loop.
     *
     * @return the next game state.
     */
    @Override
    public String Run() {
        Initialize();
        LoadContent();

        while(isRunning)
        {
            Render();
            Update();
        }

        Dispose();
        return "Menu";
    }

    /**
     * Read options from a file (Format: "option=wert;")
     * @param fileName file path
     * @return Returns a map of the option
     */
    public static Map<String, String> GetOptions(String fileName)
    {
        File tmpFile = new File(fileName);
        if (!tmpFile.exists())
        {
            try {
                tmpFile.createNewFile();
            } catch (IOException e) { }
            FileFunctions.writeFile(fileName, "AIlevel=strong;MapSize=10:10;");
        }

        Map<String, String> res = new HashMap<String, String>();
        String text = "";

        text = FileFunctions.readFile(fileName);

        String[] options = text.split(";");
        for (String option : options)
        {
            if (option.contains("="))
            {
                String[] tmp = option.split("=");
                res.put(tmp[0], tmp[1]);
            }
        }

        return res;
    }

    /**
     * Create options file from a map
     * @param fileName File path
     * @param options Options as a Map<String, String>
     */
    public static void SetOptions(String fileName, Map<String, String> options)
    {
        String text = "";
        for(String key : options.keySet())
        {
            text += key + "=" + options.get(key) + ";";
        }
        FileFunctions.writeFile(fileName, text);
    }

    /**
     * Handles game updates.
     */
    @Override
    public void Update() {
        System.out.printf("\ne = exit\n" +
                "What would you change: ");
        String input = scan.nextLine();
        int chosen;
        while (true)
        {
            chosen = 0;
            if (TryParse.CanParseInt(input))
            {
                chosen = Integer.parseInt(input);
                break;
            }
            else if (input.equalsIgnoreCase("e"))
            {
                isRunning = false;
                break;
            }
            else
            {
                System.out.printf("\nChoose a right entry.\n");
            }
        }

        if (chosen == 1)
        {
            System.out.printf("\nPlease enter ai strength (hard/weak): ");
            input = scan.nextLine();

            if (input.equalsIgnoreCase("hard"))
            { AIlevel = AILevel.Hard; }
            else if (input.equalsIgnoreCase("weak"))
            { AIlevel = AILevel.Weak; }
            else
            { System.out.printf("\nWrite right.\n"); }
        }
        else if (chosen == 2)
        {
            System.out.printf("\nEnter the new map size (min. 10): ");
            input = scan.nextLine();
            if (TryParse.CanParseInt(input) && Integer.parseInt(input) > 9)
                MapSize.setLocation(Integer.parseInt(input), Integer.parseInt(input));
            else
                System.out.printf("\nThis wasn't a right size.\n");
        }
    }

    /**
     * Handles render calls.
     */
    @Override
    public void Render() {
        System.out.printf("1.) AI Level: " + AIlevel.name() + "\n");
        System.out.printf("2.) MapSize: " + MapSize.x + "\n");
    }

    /**
     * Dispose the class and release objects.
     */
    @Override
    public void Dispose() {
        Map<String, String> options = new HashMap<String, String>();

        options.put("AIlevel", AIlevel.name());
        options.put("MapSize", MapSize.x + ":" + MapSize.y);

        SetOptions("./options.txt", options);
    }
}
