public class Main
{
    /**
     * Start the <code>GameManager</code>.
     * @param args start arguments
     */
    public static void main(String[] args) {
        // Runs the game manager.
        GameManager.getInstance().Run();
    }
}
