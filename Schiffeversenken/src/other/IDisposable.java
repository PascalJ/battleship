/**
 * Make the class Disposable
 *
 * User: Pascal
 * Version: 1.0
 * Date: 01.03.13
 *
 * Changes:
 * 1.0 (01.03.13)
 *  - First implementation
 *  - Add void 'Dispose()'
 */
package other;

/**
 * Make the class Disposable
 *
 * @author Pascal
 * @version 1.0
 */
public interface IDisposable {

    /**
     * Dispose the class and release objects.
     */
    void Dispose();
}
