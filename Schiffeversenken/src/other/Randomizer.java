/**
 * Description
 *
 * User: Pascal Johann
 * Version: 1.0
 * Date: 25.03.13
 *
 * Changes:
 * 1.0 (25.03.13)
 *  - First implementation
 *  - Add random methods nextInt(low, high) and nextInt(high)
 */
package other;

/**
 * Description
 *
 * @author Pascal Johann
 * @version 1.0
 */
public class Randomizer
{
    /**
     * Generates random numbers from low to high
     * @param low The lowest generating number.
     * @param high The highest generating number.
     * @return Returns a random number between low and high as integer.
     */
    public static Integer nextInt(int low, int high)
    {
        high++;
        return  (int) (Math.random() * (high - low) + low);
    }

    /**
     * Generates random numbers from 0 to high
     * @param high The highest generating number.
     * @return Returns a random number between 0 and high as integer.
     */
    public static Integer nextInt(int high)
    {
        return nextInt(0, high);
    }
}
