/**
 * Constants which can be used all over the project. Especially for debug output
 * or formatting unicode chars.
 *
 * User: Pascal
 * Version: 1.1
 * Date: 18.04.13
 *
 * Changes:
 * 1.0 (15.04.13)
 *  - First implementation
 *  - Add Debug and character constants.
 * 1.1 (18.04.13)
 *  - Add new Diamond character constant.
 *  - Replace smallRectangle with circle.
 */
package other;

/**
 * Description
 *
 * @author Pascal
 * @version 1.1
 */
public class Const
{
    /**
     * Use for Debug output.
     */
    public static final boolean DEBUG = false;

    /**
     * Set unicode constants for letters which can't display normally by the windows console.
     */
    // ● U+25CF
    public static final Character Circle = '\u25CF';
    // ♦ U+2666
    public static final Character Diamond = '\u2666';
    // █ U+25A0
    public static final Character Rectangle = '\u25A0';
    // ░ U+2591
    public static final Character Filter = '\u2591';
    // ═ U+2550
    public static final Character DLineH = '\u2550';
    // ╬ U+256C
    public static final Character DCross = '\u256C';
    // ╣ U+2563
    public static final Character DCrossL = '\u2563';
    // ╚ U+255A
    public static final Character DCornerUR = '\u255A';
    // ╩ U+2569
    public static final Character DCrossU = '\u2569';
    // ║ U+2551
    public static final Character DLineV = '\u2551';
    // ╝ U+255D
    public static final Character DCornerUL = '\u255D';
    // ╔ U+2554
    public static final Character DCornerRD = '\u2554';
    // ╗ U+2557
    public static final Character DCornerLD = '\u2557';
}
