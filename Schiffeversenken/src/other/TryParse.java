/**
 * A parser class to try parsing strings in different
 * data types.
 *
 * User: Pascal
 * Version: 1.1
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 * 1.0 (3.03.13)
 *  - Add methods for parsing strings to double or int
 *     - two function. one try to parse and the other say if it can be parsed
 * 1.1 (18.04.13)
 *  - Add method to check a character for parsing.
 */
package other;

/**
 * A parser class to try parsing strings in different
 * data types.
 *
 * @author Pascal
 * @version 1.1
 */
public class TryParse
{
    /**
     * Try parsing a string to an integer.
     * @param value The string to parse
     * @return Return parsed value or -1 if it fails.
     */
    public static Integer tryParseInt(String value)
    {
        if (CanParseInt(value))
            return Integer.parseInt(value);
        else
            return -1;
    }

    /**
     * Checks if the string can parsed to an integer.
     * @param value The string that should be checked.
     * @return true = can parse, false = can not parse
     */
    public static boolean CanParseInt(String value)
    {
        try
        {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException nfe)
        {
            return false;
        }
    }

    /**
     * Checks if the string can parsed to an integer.
     * @param value The char that should be checked.
     * @return true = can parse, false = can not parse
     */
    public static boolean CanParseInt(Character value)
    {
        try
        {
            Integer.parseInt(value.toString());
            return true;
        } catch (NumberFormatException nfe)
        {
            return false;
        }
    }

    /**
     * Try parsing a string to a double.
     * @param value The string to parse
     * @return Return parsed value or -1.0 if it fails.
     */
    public static Double tryParseDouble(String value)
    {
        if (CanParseDouble(value))
            return Double.parseDouble(value);
        else
            return -1.0;
    }

    /**
     * Checks if the string can parsed to a double.
     * @param value The string that should be checked.
     * @return true = can parse, false = can not parse
     */
    public static boolean CanParseDouble(String value)
    {
        try
        {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException nfe)
        {
            return false;
        }
    }
}
