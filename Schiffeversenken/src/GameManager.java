/** GameManager
 * The game manager control and coordinate all game relevant classes.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (28.02.13)
 *  - First implementation
 *  - implements singleton pattern.
 * 1.0 (18.04.13)
 *  - Refactoring
 */
import game.*;

import java.util.HashMap;
import java.util.Map;

/** GameManager
 * The game manager control and coordinate all game relevant classes.
 *
 * @author Pascal
 * @version 1.0
 */
public class GameManager
implements IGame
{
    public boolean IsGameRunning;
    public Map<String, IGame> gameStates;
    String gameState;

    /** The instance property. */
    private static GameManager ourInstance = new GameManager();

    /**
     * @return Get the singleton instance of the gameManager.
     */
    public static GameManager getInstance() {
        return ourInstance;
    }

    /**
     * Initialize the GameManager
     */
    private GameManager() {
    }

    /**
     * Initialize the game object.
     */
    public void Initialize() {
        IsGameRunning = true;
        gameStates = new HashMap<String, IGame>();
        gameState = "Menu";
    }

    /**
     * Load the game content.
     */
    public void LoadContent() {
        gameStates.put("Menu", new Menu());
        gameStates.put("Options", new Options());
        gameStates.put("Game", new Game());
    }

    /**
     * Initializes and start a rudimentary game loop.
     */
    public String Run() {
        Initialize();
        LoadContent();

        while (IsGameRunning)
        {
            Render();
            Update();
        }

        Dispose();
        return "Exit";
    }

    /**
     * Handles game updates.
     */
    public void Update() {
        if (gameState.equalsIgnoreCase("exit"))
        {
           System.out.printf("\nBye. Hope you will come back. :)\n\n");
            IsGameRunning = false;
        }
    }

    /**
     * Handles render calls.
     */
    public void Render() {
        gameState = gameStates.get(gameState).Run();
    }

    /**
     * Dispose the class and release objects.
     */
    public void Dispose()
    {
        // Dispose each game state.
        gameStates.get("Menu").Dispose();
        gameStates.get("Options").Dispose();
        gameStates.get("Game").Dispose();

        gameStates.clear();
    }
}
