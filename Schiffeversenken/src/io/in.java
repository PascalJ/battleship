/**
 * A class with static function and methods to control the
 * user input.
 *
 * User: Pascal
 * Version: 1.2
 * Date: 18.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 *  - Add getMenuInput() function.
 * 1.0 (13.04.13)
 *  - Add TranslateMapCoordinates()
 *  - Add TranslateStringCoordinate()
 * 1.1 (18.04.13)
 *  - Fix some bugs
 */
package io;

import other.TryParse;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * A class with static function and methods to control the
 * user input.
 *
 * @author Pascal
 * @version 1.2
 */
public class in
{
    public static Scanner scanner = new Scanner(System.in);

    /**
     * Locate the chosen menu entry
     * @param menu List of menu entries.
     * @param repeatOnError if it's true you can not leave the menu until you choose a valid menu entry
     * @return The chosen menu entry,
     */
    public static Map<String, Integer> getMenuInput(List<String> menu, boolean repeatOnError)
    {
        Map<String, Integer> res = new HashMap<String, Integer>();

        while (true)
        {
            String input = scanner.nextLine();
            if (TryParse.CanParseInt(input) && !input.equals(""))
            {
                Integer tmp = Integer.parseInt(input) - 1;
                if (menu.size() > tmp)
                {
                    res.put(menu.get(tmp), tmp);
                    return res;
                }
                else
                    System.out.printf(" -- No valid input, please try again: ");
            }
            else
            {
                if (!repeatOnError)
                {
                    res.put("none", -1);
                    return res;
                }
                else
                    System.out.printf(" -- No valid input, please try again: ");
            }
        }
    }

    /**
     * Translate an input coordinate, per example 'A2' into a coordinate which can be used for a List
     * 'A2' becomes to a point variable with x = 1 and y = 2.
     * Can translate a maximal map size of 676x676 (because i process only 2 letters and this letters
     * can have 676 combinations).
     *
     * @param input A Map coordinate like this: 'A2'
     * @return Returns a point variable where the char becomes the x coordinate and the number the y coordinate.
     */
    public static Point TranslateMapCoordinates(String input)
    {
        Point coordinates = new Point(0, 0);
        String tempX = "";
        Integer x = 0;
        String y = "";

        for (int i = 0; i < input.length(); i++)
        {
            if (TryParse.CanParseInt(input.charAt(i)))
                y += input.charAt(i);
            else
                tempX += input.charAt(i);
        }

        for (int i = 0; i < tempX.length(); i++)
        {
            if (i == 0)
            {
                x += TranslateStringCoordinate(tempX.charAt(0));
            }
            else if (i == 1)
            {
                // * 26 for the alphabet.
                x = TranslateStringCoordinate(tempX.charAt(0)) * 26;
                x = x + TranslateStringCoordinate(tempX.charAt(1));
            }
        }

        if (x != 0 && !y.equals(""))
            coordinates.setLocation(x, Integer.parseInt(y));
        else
            coordinates.setLocation(-1, -1);

        return coordinates;
    }

    /**
     * Translate string coordinates into number coordinates which can be used on a list.
     *
     * @param input char which should be translated
     * @return Translate a-z (or A-Z) to 1-26.
     */
    private static Integer TranslateStringCoordinate(Character input)
    {
        String tmp = input.toString();
        Character ret = tmp.toUpperCase().charAt(0);
        return ret - 64;
    }
}
