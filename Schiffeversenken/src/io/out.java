/**
 * A class with static functions and methods to format the
 * output.
 *
 * User: Pascal
 * Version: 1.0
 * Date: 13.04.13
 *
 * Changes:
 * 0.1 (01.03.13)
 *  - First implementation
 *  - Add WriteMenu method.
 * 1.0 (13.04.13)
 *  - Add TranslateNumberCoordinate()
 */
package io;

import java.util.List;

/**
 * A class with static functions and methods to format the
 * output.
 *
 * @author Pascal
 * @version 1.0
 */
public class out
{
    /**
     * Write a well formatted menu to the console.
     * @param menu List of menu entries.
     */
    public static void WriteMenu(List<String> menu)
    {
        for (int i = 0; i < menu.size(); i++)
        {
            System.out.println(i+1 + ".) " + menu.get(i));
        }
        System.out.printf("\nType your choose: ");
    }

    /**
     * Translate number coordinates into string coordinates which can be used for the map output.
     *
     * @param input Integer which should be translated (currently 1-26)
     * @return Translate 1-26 to a-z.
     */
    public static String TranslateNumberCoordinate(int input)
    {
        Character ret = (char)(input + 64);
        return ret.toString();
    }
}
