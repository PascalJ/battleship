/**
 * Class for file input and output
 *
 * Author: Pascal
 * Date: 18.04.2013
 * Version: 1.0
 *
 ** Changes:
 * 1.0 (18.04.2013):
 *   - First implementation
 *   - Read & write text files
 */
package io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class for file input and output
 *
 * @author Pascal
 * @version 1.0
 */
public class FileFunctions {

    /**
     * Reads a specific file.
     * @param fileName Save path of the file.
     * @throws IOException
     */
    public static String readFile(String fileName)
    {
        byte chars;
        String text = "";

        try
        {
            FileInputStream readStream = new FileInputStream(fileName);
            do
            {
                chars = (byte)readStream.read();
                text += (char)chars;
            } while (chars != -1);
            readStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

    /**
     * Saves a file at a specific place
     * @param fileName Save path.
     * @param text Content of the text.
     */
    public static void writeFile(String fileName, String text)
    {
        try
        {
            FileOutputStream writeStream;
            writeStream = new FileOutputStream(fileName);

            for (int i = 0; i < text.length(); i++)
            {
                writeStream.write((byte)text.charAt(i));
            }
            writeStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
